package com.hi.cryptoquotes.controller.converter;

import com.hi.cryptoquotes.controller.dto.response.HistoricalQuotesDTO;
import com.hi.cryptoquotes.domain.CryptoStock;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

/**
 * Converts a CryptoStock from the internal domain into a Crypto Historical Quotes DTO.
 */
@Component
public class HistoricalQuotesDtoConverter implements Function<List<CryptoStock>, HistoricalQuotesDTO> {
    @Override
    public HistoricalQuotesDTO apply(List<CryptoStock> cryptoQuotes) {
        HistoricalQuotesDTO historicalQuotesDTO = new HistoricalQuotesDTO();
        if (!cryptoQuotes.isEmpty()) {
            historicalQuotesDTO.setSymbol(cryptoQuotes.get(0).getSymbol());
            historicalQuotesDTO.setName(cryptoQuotes.get(0).getName());
        }
        Iterator<CryptoStock> cryptoIterator = cryptoQuotes.iterator();
        while (cryptoIterator.hasNext()) {
            CryptoStock cryptoTicker = cryptoIterator.next();
            historicalQuotesDTO.getQuoteHistory().add(cryptoTicker.getQuote());
        }
        return historicalQuotesDTO;
    }
}