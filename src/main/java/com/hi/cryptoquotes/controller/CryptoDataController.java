package com.hi.cryptoquotes.controller;

import com.hi.cryptoquotes.controller.converter.HistoricalQuotesDtoConverter;
import com.hi.cryptoquotes.controller.dto.response.HistoricalQuotesDTO;
import com.hi.cryptoquotes.domain.CryptoStock;
import com.hi.cryptoquotes.service.CryptoDataService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@Api(tags="Crypto data API")
@RequestMapping("/api/v1")
public class CryptoDataController {
    private final CryptoDataService cryptoDataService;
    private final HistoricalQuotesDtoConverter historicalQuotesDtoConverter;

    @Autowired
    public CryptoDataController(CryptoDataService cryptoDataService, HistoricalQuotesDtoConverter historicalQuotesDtoConverter) {
        this.cryptoDataService = cryptoDataService;
        this.historicalQuotesDtoConverter = historicalQuotesDtoConverter;
    }

    @GetMapping(path = "/crypto/quotes/latest")
    public List<CryptoStock> getLatestQuotes() {
        return cryptoDataService.getLatestStockQuotes();
    }

    @GetMapping(path = "/crypto/quotes/historical")
    public HistoricalQuotesDTO getHistoricalQuotes(
            @RequestParam(required = true) final String symbol,
            @RequestParam(required = true)@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH") final Date fromDate,
            @RequestParam(required = true)@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH") final Date toDate) {
        List<CryptoStock> cryptoQuoteList = cryptoDataService.getHistoricalStockQuotes(symbol, fromDate, toDate);
        return this.historicalQuotesDtoConverter.apply(cryptoQuoteList);
    }
}