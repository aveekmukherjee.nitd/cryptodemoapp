package com.hi.cryptoquotes.controller.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hi.cryptoquotes.domain.Quote;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.ArrayList;
import java.util.List;

public class HistoricalQuotesDTO {
    @JsonProperty("symbol")
    private String symbol;
    @Field(type = FieldType.Keyword)
    @JsonProperty("name")
    private String name;
    @Field(type = FieldType.Integer)
    @JsonProperty("rank")
    private int rank;
    @JsonProperty("quotes")
    List<Quote> quoteHistory;

    public HistoricalQuotesDTO() {
        this.quoteHistory = new ArrayList<>();
    }

    public List<Quote> getQuoteHistory() {
        return quoteHistory;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}
