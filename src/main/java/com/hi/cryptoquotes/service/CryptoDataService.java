package com.hi.cryptoquotes.service;

import com.hi.cryptoquotes.domain.CryptoStock;
import java.util.Date;
import java.util.List;

public interface CryptoDataService {
    /**
     * Retrieves the latest price of all major stocks dictated by the predicate
     * @return
     */
    List<CryptoStock> getLatestStockQuotes();
    List<CryptoStock> getHistoricalStockQuotes(String symbol, Date fromDt, Date toDt);
}
