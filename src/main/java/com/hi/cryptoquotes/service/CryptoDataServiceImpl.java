package com.hi.cryptoquotes.service;

import com.google.common.collect.Lists;
import com.hi.cryptoquotes.domain.CryptoStock;
import com.hi.cryptoquotes.repository.CryptoRepository;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
public class CryptoDataServiceImpl implements CryptoDataService {
    @Autowired
    private final CryptoRepository cryptoRepository;

    private final RestHighLevelClient esClient;

    public CryptoDataServiceImpl(CryptoRepository cryptoTickerRepository, RestHighLevelClient esClient) {
        this.cryptoRepository = cryptoTickerRepository;
        this.esClient = esClient;
    }

    /**
     * Returns latest crypto quotes based on UTC time of 1 hour previous, sorted by crypto rank
     * @return list of crypto quotes sorted by rank
     */
    @Override
    public List<CryptoStock> getLatestStockQuotes() {
        Date dateNow = new Date(Instant.now().minus(1, ChronoUnit.DAYS).toEpochMilli());
        Iterable<CryptoStock> resultSortedByTime = cryptoRepository.findCryptoQuotesAfterGivenDate(dateNow, "rank", SortOrder.ASC);
        return Lists.newArrayList(resultSortedByTime);
    }

    /**
     * Returns historical stocks
     * @param symbol symbol of crypto
     * @param from start date
     * @param to end date
     * @return quote history within the specified interval
     */
    @Override
    public List<CryptoStock> getHistoricalStockQuotes(final String symbol, final Date from, final Date to) {
        return getHistoricalStockQuotes(symbol, from, to, 0, 100);
    }

    /**
     * Returns historical stocks sorted by timestamp
     * @param symbol crypto symbol
     * @param from start date of interval
     * @param to end date of interval
     * @param pageStart page offset
     * @param pageLimit page size
     * @return list of crypto quotes sorted by timestamp
     */
    public List<CryptoStock> getHistoricalStockQuotes(final String symbol, final Date from, final Date to, int pageStart, int pageLimit) {
        Pageable pageRequest = PageRequest.of(pageStart, pageLimit);
        Iterable<CryptoStock> pagedResult = cryptoRepository.findAllWithinDateInterval(
                symbol, from, to, pageRequest);
        return Lists.newArrayList(pagedResult);
    }
}
