package com.hi.cryptoquotes.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class HistoricalQuote {
    @JsonProperty("quotes")
    List<Quote> quoteHistory;

    public List<Quote> getQuoteHistory() {
        return quoteHistory;
    }
}
