package com.hi.cryptoquotes.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hi.cryptoquotes.domain.constants.Indices;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Objects;

@Document(indexName = Indices.CRYPTO_HOURLY_IDX)
public class CryptoStock {
    @Id
    @Field(type = FieldType.Keyword)
    @JsonProperty("symbol")
    private String symbol;
    @Field(type = FieldType.Keyword)
    @JsonProperty("name")
    private String name;
    @Field(type = FieldType.Integer)
    @JsonProperty("rank")
    private int rank;
    @Field(type = FieldType.Nested)
    @JsonProperty("quote")
    private Quote quote;

    public CryptoStock(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getName() {
        return name;
    }

    public int getRank() {
        return rank;
    }

    public Quote getQuote() {
        return quote;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CryptoStock that = (CryptoStock) o;
        return Objects.equals(symbol, that.symbol) && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(symbol, name);
    }
}
