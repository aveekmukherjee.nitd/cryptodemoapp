package com.hi.cryptoquotes.domain;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;

public class Quote {
    @Field(type = FieldType.Double)
    @JsonProperty("price")
    private Double price;
    @Field(type = FieldType.Text)
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("timestamp")
    @Field(type = FieldType.Date)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private Date timestamp;

    public Double getPrice() {
        return price;
    }

    public String getCurrency() {
        return currency;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
