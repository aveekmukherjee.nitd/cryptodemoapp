package com.hi.cryptoquotes.config;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackages = "com.hi.cryptoquotes.repository")
@ComponentScan(basePackages = "com.hi.cryptoquotes")
public class ElasticSearchConfig extends AbstractElasticsearchConfiguration {
    @Value("${elasticsearch.url}")
    public String elasticSearchUrl;
    @Value("${elasticsearch.user}")
    public String esUser;
    @Value("${elasticsearch.password}")
    public String esPass;

    @Override
    @Bean
    public RestHighLevelClient elasticsearchClient() {
        final ClientConfiguration config = ClientConfiguration.builder()
                .connectedTo(elasticSearchUrl)
                .withBasicAuth(esUser, esPass)
                .build();
        System.out.println(config.getEndpoints());
        return RestClients.create(config).rest();
    }
}