package com.hi.cryptoquotes.repository.util;

import com.hi.cryptoquotes.domain.CryptoStock;
import java.util.Comparator;


public final class ComparatorsUtil {
    public static final Comparator<CryptoStock> CRYPTO_STOCK_RANK_COMPARATOR =
            Comparator.comparingInt(CryptoStock::getRank);
}
