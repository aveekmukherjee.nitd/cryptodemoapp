package com.hi.cryptoquotes.repository;

import com.hi.cryptoquotes.domain.CryptoStock;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CryptoRepository extends ElasticsearchRepository<CryptoStock, String>, CryptoRepositoryCustom {
}
