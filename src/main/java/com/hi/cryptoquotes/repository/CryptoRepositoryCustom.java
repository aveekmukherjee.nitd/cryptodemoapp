package com.hi.cryptoquotes.repository;

import com.hi.cryptoquotes.domain.CryptoStock;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;

public interface CryptoRepositoryCustom {
    Page<CryptoStock> findAllWithinDateInterval(String symbol, Date startDt, Date endt, Pageable pageable);
    Iterable<CryptoStock> findCryptoQuotesAfterGivenDate(Date startDt, String sortBy, SortOrder sortOrder);
    Page<CryptoStock> findCryptoQuotesAfterGivenDate(Date dateNow, String sortBy, SortOrder sortOrder, Pageable pageable);
}
