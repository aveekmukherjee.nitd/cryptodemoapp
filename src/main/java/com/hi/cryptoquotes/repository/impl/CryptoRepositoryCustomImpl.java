package com.hi.cryptoquotes.repository.impl;

import com.hi.cryptoquotes.domain.CryptoStock;
import com.hi.cryptoquotes.repository.CryptoRepositoryCustom;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.*;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.util.Assert;

import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.index.query.QueryBuilders.rangeQuery;
import static com.hi.cryptoquotes.repository.util.ComparatorsUtil.CRYPTO_STOCK_RANK_COMPARATOR;

public class CryptoRepositoryCustomImpl implements CryptoRepositoryCustom {
    private final ElasticsearchOperations operations;

    public CryptoRepositoryCustomImpl(ElasticsearchOperations operations) {
        this.operations = operations;
    }

    @Override
    public Page<CryptoStock> findAllWithinDateInterval(String symbol, Date startDt, Date endDt, Pageable pageable) {
        Assert.notNull(symbol, "Cannot use 'null' symbol.");
        Assert.notNull(startDt, "start date cannot be 'null'.");
        Assert.notNull(endDt, "end date cannot be 'null'.");

        NativeSearchQuery query = new NativeSearchQueryBuilder()
                .withQuery(boolQuery()
                        .filter( rangeQuery("quote.timestamp")
                                .from(startDt)
                                .to(endDt))
                        .must(matchQuery("symbol", symbol)))
                .withPageable(pageable)
                .build();
        SearchHits<CryptoStock> searchHits = operations.search(query, CryptoStock.class);
        SearchPage<CryptoStock> page = SearchHitSupport.searchPageFor(searchHits, query.getPageable());
        return (Page<CryptoStock>) SearchHitSupport.unwrapSearchHits(page);
    }

    @Override
    public Iterable<CryptoStock> findCryptoQuotesAfterGivenDate(Date startDt, String sortBy, SortOrder sortOrder) {
        Assert.notNull(startDt, "from date cannot be 'null'.");
        Assert.notNull(sortOrder, "sort Order cannot be 'null'.");
        NativeSearchQuery query = new NativeSearchQueryBuilder()
                .withQuery(rangeQuery("quote.timestamp").gte(startDt))
                .withSorts(SortBuilders.fieldSort(sortBy).order(sortOrder))
                .withSorts(SortBuilders.fieldSort("quote.timestamp").order(SortOrder.DESC))
                .build();
        SearchHits<CryptoStock> searchHits = operations.search(query, CryptoStock.class);
        List<CryptoStock> cryptoStocks = (List<CryptoStock>) SearchHitSupport.unwrapSearchHits(searchHits);
        Set<CryptoStock> latestQuotes = new TreeSet<>(CRYPTO_STOCK_RANK_COMPARATOR);
        for (CryptoStock cryptoStock: cryptoStocks) latestQuotes.add(cryptoStock);
        return new ArrayList<>(latestQuotes);
    }

    @Override
    public Page<CryptoStock> findCryptoQuotesAfterGivenDate(Date startDt, String sortBy, SortOrder sortOrder, Pageable pageable) {
        Assert.notNull(startDt, "from date cannot be 'null'.");
        Assert.notNull(sortOrder, "sort Order cannot be 'null'.");
        Assert.notNull(pageable, "page attr cannot be 'null'.");
        NativeSearchQuery query = new NativeSearchQueryBuilder()
                .withQuery(rangeQuery("quote.timestamp").gte(startDt))
                .withSorts(SortBuilders.fieldSort(sortBy).order(sortOrder))
                .withSorts(SortBuilders.fieldSort("quote.timestamp").order(SortOrder.DESC))
                .withPageable(pageable)
                .build();
        SearchHits<CryptoStock> searchHits = operations.search(query, CryptoStock.class);
        SearchPage<CryptoStock> page = SearchHitSupport.searchPageFor(searchHits, query.getPageable());
        return (Page<CryptoStock>) SearchHitSupport.unwrapSearchHits(page);
    }
}
